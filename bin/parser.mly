%{
  open Ast
  open Ast.Rt
  open Ast.Event
%}

%token EOF LPAREN RPAREN LBRACKET RBRACKET COMMA TASK
%token<int> NUM
%token<string> ID AFFINITY INTERVALS
%token<string*int> PERIOD
%token<string*string> SUCC

%start<Rt.t> rt
%start<Affinity.t> affinity
%start<Event.t> events
%start<string list> query_results
%start<Iv.interval list> intervals

%%

affinity: a=affinity_line+ EOF { a }

affinity_line: task=AFFINITY core=NUM { task, core }

rt: tasks=task+ EOF { tasks }

task: p=PERIOD transitions=transition+
{
  let graph =
    List.fold_left
      (fun acc (s1, s2) ->
        StringMap.update s1
          (function None -> Some [ s2 ] | Some s -> Some (s2 :: s))
          acc)
      StringMap.empty transitions
  in
  (fst p, { period = snd p; graph })
}

transition:
| s=SUCC
{
  let s1, s2 = s in
  if String.starts_with ~prefix:"loop_" s2 then
    (s1, (true, String.sub s2 5 (String.length s2 - 5)))
  else if String.starts_with ~prefix:"END_" s2 then
    (s1, (false, "end"))
  else
    (s1, (false, s2))
}

events: tasks=task_event+ EOF { tasks }

task_event: TASK tname=ID segments=segment_event+ { (tname, segments) }

segment_event: sname=ID event=event+ { (sname, event) }

event: LPAREN name=ID COMMA LBRACKET min=NUM COMMA max=NUM RBRACKET RPAREN { {name; min; max} }

query_results: intervals=query_result+ EOF { intervals }

query_result: intervals=INTERVALS { intervals }

intervals: intervals=separated_nonempty_list(COMMA, interval) EOF { intervals }

interval:
| LBRACKET min=NUM COMMA max=NUM RBRACKET { { min; max } }
| n=NUM                                   { { min = n; max = n } }
