open Ast
open Xta
open Rt
open Event

let delta_rb se j = (List.nth se j).max - (List.nth se 0).max
let delta_lb se j = (List.nth se j).min - (List.nth se 0).min
let max_delay se j = (List.nth se j).max - (List.nth se (j - 1)).min
let min_delay se j = (List.nth se j).min - (List.nth se (j - 1)).max

let state1 s k iv_s_e1_k =
  {
    name = Format.asprintf "%s_%d" s (k + 1);
    invariant =
      [
        Format.asprintf "x <= %d"
          (List.nth iv_s_e1_k (List.length iv_s_e1_k - 1)).Iv.max;
      ];
  }

let state2 s k i j iv_s_e1_k se =
  let invariantX =
    Format.asprintf "x <= %d" ((List.nth iv_s_e1_k i).Iv.max + delta_rb se j)
  in
  let invariantY = Format.asprintf "y <= %d" (max_delay se j) in
  let iv = List.nth iv_s_e1_k i in
  {
    name = Format.asprintf "%s_%d_%d_%d" s (k + 1) (i + 1) (j + 1);
    invariant =
      (if iv.min = iv.max then [ invariantY ] else [ invariantX; invariantY ]);
  }

let transition1 s k iv succ se reset maxX =
  let guardMin = Format.asprintf "x >= %d" iv.Iv.min in
  let guardMax = Format.asprintf "x <= %d" iv.max in
  {
    src = Format.asprintf "%s_%d" s (k + 1);
    dst = succ;
    guard = (if maxX then [ guardMin; guardMax ] else [ guardMin ]);
    sync = Some (Format.asprintf "%s!" (List.hd se).name);
    update = reset;
  }

let transition2 s k i j iv succ se reset =
  let guardX = Format.asprintf "x >= %d" (iv.Iv.min + delta_lb se j) in
  let guardY = Format.asprintf "y >= %d" (min_delay se j) in
  {
    src = Format.asprintf "%s_%d_%d_%d" s (k + 1) (i + 1) (j + 1);
    dst = succ;
    guard = (if iv.min = iv.max then [ guardY ] else [ guardX; guardY ]);
    sync = Some (Format.asprintf "%s!" (List.nth se j).name);
    update = reset;
  }

(* locations of the form s_k_i_j *)
let locations2 s k iv_s_e1_k se =
  List.mapi
    (fun i _iv ->
      List.mapi
        (fun j _event ->
          let j = j + 1 in
          state2 s k i j iv_s_e1_k se)
        (List.tl se))
    iv_s_e1_k

(* all locations *)
let segment_locations jset hpc period iv =
  List.map
    (fun (s, se) ->
      let iv_s_e1 = StringMap.find s iv |> StringMap.find (List.hd se).name in
      List.init (hpc / period) (fun k ->
          let iv_s_e1_k = List.nth iv_s_e1 k in
          let location1 = state1 s k iv_s_e1_k in
          let location2 = locations2 s k iv_s_e1_k se in
          location1 :: List.concat location2))
    jset
  |> List.concat |> List.concat

(* edges of the form s_k_i_j-> for one job *)
let edge2 s k i hpc period iv se =
  List.mapi
    (fun j _event ->
      let j = j + 1 in
      let succ, reset =
        if j <> List.length se - 1 then
          (Format.asprintf "%s_%d_%d_%d" s k (i + 1) (j + 2), [ "y" ])
        else if k <> (hpc / period) - 1 then
          (Format.asprintf "%s_%d" s (k + 2), [])
        else ("wait", [])
      in
      transition2 s k i j iv succ se reset)
    (List.tl se)

(* all edges for one job *)
let same_segment_edges jset hpc period iv =
  List.map
    (fun (s, se) ->
      let iv_s_e1 = StringMap.find s iv |> StringMap.find (List.hd se).name in
      List.init (hpc / period) (fun k ->
          let iv_s_e1_k = List.nth iv_s_e1 k in
          let guardMax = List.length iv_s_e1_k > 1 in
          List.mapi
            (fun i iv ->
              let succ, reset =
                if List.length se <> 1 then
                  (Format.asprintf "%s_%d_%d_2" s (k + 1) (i + 1), [ "y" ])
                else if k <> (hpc / period) - 1 then
                  (Format.asprintf "%s_%d" s (k + 2), [])
                else ("wait", [])
              in
              let edge1 = transition1 s k iv succ se reset guardMax in
              let edge2 = edge2 s k i hpc period iv se in
              edge1 :: edge2)
            iv_s_e1_k))
    jset
  |> List.concat |> List.concat |> List.concat

(* edges between segments for multiple job *)
let different_segment_edges jset hpc period iv =
  List.map
    (fun (s, se) ->
      let iv_s_e1 = StringMap.find s iv |> StringMap.find (List.hd se).name in
      List.init
        ((hpc / period) - 1)
        (fun k ->
          let iv_s_e1_k = List.nth iv_s_e1 k in
          List.mapi
            (fun i iv ->
              List.fold_left
                (fun acc (s', _se) ->
                  if s <> s' then
                    let succ = Format.asprintf "%s_%d" s' (k + 2) in
                    let edge =
                      if List.length se = 1 then
                        transition1 s k iv succ se [] false
                      else transition2 s k i (List.length se) iv succ se []
                    in
                    edge :: acc
                  else acc)
                [] jset
              |> List.rev)
            iv_s_e1_k))
    jset
  |> List.concat |> List.concat |> List.concat

let task_abstraction hpc period name jset iv =
  let clocks =
    if List.exists (fun (_s, se) -> List.length se > 1) jset then [ "x"; "y" ]
    else [ "x" ]
  in

  let states =
    let wait =
      { name = "wait"; invariant = [ Format.asprintf "x <= %d" hpc ] }
    in
    let act =
      if List.length jset = 1 then [] else [ { name = "act"; invariant = [] } ]
    in
    let segment_locations = segment_locations jset hpc period iv in
    (wait :: act) @ segment_locations
  in

  let commit = if List.length jset = 1 then [] else [ "act" ] in

  let init =
    if List.length jset = 1 then Format.asprintf "%s_1" (List.hd jset |> fst)
    else "act"
  in

  let trans =
    let from_act =
      if List.length jset = 1 then []
      else
        List.map
          (fun (s, _se) ->
            {
              src = "act";
              dst = Format.asprintf "%s_1" s;
              guard = [];
              sync = None;
              update = [];
            })
          jset
    in
    let trans =
      same_segment_edges jset hpc period iv
      @ different_segment_edges jset hpc period iv
    in
    let from_wait =
      let succ =
        if List.length jset = 1 then Format.asprintf "%s_1" (fst (List.hd jset))
        else "act"
      in
      {
        src = "wait";
        dst = succ;
        guard = [ Format.asprintf "x == %d" hpc ];
        sync = None;
        update = [ "x" ];
      }
    in
    from_act @ trans @ [ from_wait ]
  in

  { name; clocks; states; commit; init; trans }

let abstraction affinity rt event iv =
  let hpc = cores_hp affinity rt in
  let channels =
    List.concat_map snd event |> List.concat_map snd
    |> List.map (fun { name; _ } -> name)
  in
  let tasks =
    List.map
      (fun (t, jset) ->
        let core = List.assoc t affinity in
        let hpc = IntMap.find core hpc in
        let task = List.assoc t rt in
        let period = task.period in
        let tiv = StringMap.find t iv in
        task_abstraction hpc period t jset tiv)
      event
  in
  { channels; tasks }
