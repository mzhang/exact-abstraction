open Ast

let check_distinct_affinity event affinity =
  Event.cores_tasks affinity event
  |> IntMap.iter (fun c tasks ->
         if List.length tasks > 1 then
           failwith
           @@ Format.asprintf
                "distinct affinity assertion failed, tasks %s are on core %d."
                (String.concat ", " tasks) c)

let check_task_event_jobs tname jobs event_segments force =
  if List.length jobs > 1 then
    Format.printf
      "Warning: %s is a multiple-job task, make sure your requirement is valid.\n"
      tname;
  List.iter
    (fun job ->
      let nb_events =
        List.filter (fun sname -> List.mem sname event_segments) job
        |> List.length
      in
      if nb_events = 0 then
        if force then
          Format.printf
            "Warning: generated abstraction may be not exact, the job %s in \
             task %s does not produce event.\n"
            (String.concat ", " job) tname
        else
          failwith
          @@ Format.asprintf
               "there exists a job with no event production: %s in task %s. \
                Use option -force to ignore error."
               (String.concat ", " job) tname
      else if nb_events > 1 then
        failwith
        @@ Format.asprintf
             "there are two segments that produce event in job %s in task %s."
             (String.concat ", " job) tname)
    jobs

let check_task_event_segment_reachable tname jobs event_segments =
  let reachable = List.concat jobs in
  List.iter
    (fun sname ->
      if sname = "act" || sname = "end" || not (List.mem sname reachable) then
        failwith
        @@ Format.asprintf "segment %s is not reachable in task %s." sname tname)
    event_segments

let check_segment_event_ordered tname sname events =
  let rec aux events last =
    match events with
    | [] -> ()
    | e :: events ->
        if
          not
            (e.Event.min <= e.max && last.Event.min <= e.min
           && last.max <= e.max)
        then
          failwith
          @@ Format.asprintf "events of segment %s in task %s are not ordered."
               sname tname
        else aux events e
  in
  aux events (List.hd events)

let check_task_event force rt (tname, task_event) =
  let task =
    match List.assoc_opt tname rt with
    | None -> failwith @@ Format.asprintf "unknown task %s." tname
    | Some task -> task
  in
  let snames, _events = List.split task_event in
  let jobs = Ast.Rt.jobs task.Rt.graph in
  check_task_event_jobs tname jobs snames force;
  check_task_event_segment_reachable tname jobs snames;
  List.iter
    (fun (sname, events) -> check_segment_event_ordered tname sname events)
    task_event

let check_event event rt force = List.iter (check_task_event force rt) event
