module StringMap = Map.Make (String)
module IntMap = Map.Make (Int)

module Affinity = struct
  type t = (string * int) list [@@deriving show]
end

module Rt = struct
  type task = {
    period : int;
    graph : (bool * string) list StringMap.t;
        [@printer
          fun fmt t ->
            StringMap.iter
              (fun s1 succs ->
                List.iter
                  (fun (loop, s2) ->
                    fprintf fmt "%s -> %s%s\n" s1
                      (if loop then "loop " else "")
                      s2)
                  succs)
              t]
  }
  [@@deriving show { with_path = false }]

  type t = (string * task) list [@@deriving show]

  let jobs graph =
    (* [paths] is paths under construction
        [jobs] is jobs accumulator
       [from_act] is successor of act accumulator *)
    let rec aux paths jobs from_act =
      match paths with
      | [] -> jobs
      | current :: paths ->
          (* last state of the current path under construction *)
          let last = List.hd current in
          let succ =
            match StringMap.find_opt last graph with
            | None ->
                failwith @@ Format.asprintf "segment %s cannot reach end" last
            | Some succ -> succ
          in
          let paths, jobs, from_act =
            List.fold_left
              (fun (paths, jobs, from_act) (loop, succ) ->
                if List.mem succ current then
                  failwith
                  @@ Format.asprintf "there is a cycle in path %s"
                       (String.concat ", " (List.rev (succ :: current)))
                else if succ = "end" then
                  (* current path is a job *)
                  let jobs = List.rev current :: jobs in
                  (paths, jobs, from_act)
                else if loop then
                  (* current path is a job *)
                  let jobs = List.rev current :: jobs in
                  (* add [succ] to paths if it is not done yet *)
                  let paths, from_act =
                    if List.mem succ from_act then (paths, from_act)
                    else ([ succ ] :: paths, succ :: from_act)
                  in
                  (paths, jobs, from_act)
                else
                  (* add succ to current path *)
                  let paths = (succ :: current) :: paths in
                  (paths, jobs, from_act))
              (paths, jobs, from_act) succ
          in
          aux paths jobs from_act
    in
    let from_act = StringMap.find "act" graph in
    let _loop, from_act = List.split from_act in
    let paths = List.map (fun s -> [ s ]) from_act in
    aux paths [] from_act

  let cores_hp affinity rt =
    let rec gcd a b = if b = 0 then a else gcd b (a mod b) in
    let lcm a b = a / gcd a b * b in
    List.fold_left
      (fun acc (tname, task) ->
        let core = List.assoc tname affinity in
        IntMap.update core
          (function
            | None -> Some task.period | Some hp -> Some (lcm hp task.period))
          acc)
      IntMap.empty rt
end

module Event = struct
  type event = { name : string; min : int; max : int }
  [@@deriving show { with_path = false }]

  type segment_event = string * event list [@@deriving show]
  type task_event = segment_event list [@@deriving show]
  type t = (string * task_event) list [@@deriving show]

  let cores_tasks affinity event =
    List.fold_left
      (fun acc (tname, _task_event) ->
        IntMap.update
          (List.assoc tname affinity)
          (function
            | None -> Some [ tname ] | Some tasks -> Some (tname :: tasks))
          acc)
      IntMap.empty event
end

module Iv = struct
  type interval = { min : int; max : int }
  [@@deriving show { with_path = false }]

  (* type for pp *)
  type interval_pp = interval list [@@deriving show]

  (* iv[task][segment][event][k][i] *)
  type t =
    (interval list list StringMap.t StringMap.t StringMap.t
    [@printer
      fun fmt t ->
        StringMap.iter
          (fun task iv ->
            fprintf fmt "\n\nTask: %s" task;
            StringMap.iter
              (fun segment iv ->
                StringMap.iter
                  (fun event iv ->
                    List.iteri
                      (fun k iv ->
                        fprintf fmt "\nIv(%s,%s,%d) = %s" segment event k
                          (show_interval_pp iv))
                      iv)
                  iv)
              iv)
          t])
  [@@deriving show]
end

module Xta = struct
  type state = { name : string; invariant : string list }

  let state_to_string (state : state) =
    state.name
    ^
    match state.invariant with
    | [] -> ""
    | inv -> " { " ^ String.concat " && " inv ^ " }"

  type transition = {
    src : string;
    dst : string;
    guard : string list;
    sync : string option;
    update : string list;
  }

  let transition_to_string trans =
    let guard =
      match trans.guard with
      | [] -> ""
      | l -> "guard " ^ String.concat " && " l ^ "; "
    in
    let sync =
      match trans.sync with None -> "" | Some sync -> "sync " ^ sync ^ "; "
    in
    let update =
      match trans.update with
      | [] -> ""
      | l ->
          "assign "
          ^ String.concat ",\n" (List.map (fun c -> c ^ " := 0") l)
          ^ "; "
    in
    trans.src ^ " -> " ^ trans.dst ^ " { " ^ guard ^ sync ^ update ^ "}"

  type process = {
    name : string;
    clocks : string list;
    states : state list;
    commit : string list;
    init : string;
    trans : transition list;
  }

  let process_to_string process =
    let clocks =
      match process.clocks with
      | [] -> ""
      | l -> "clock " ^ String.concat ", " l ^ ";\n"
    in
    let states =
      match process.states with
      | [] -> ""
      | l ->
          "state\n    "
          ^ (List.map state_to_string l |> String.concat ",\n    ")
          ^ ";\n"
    in
    let commit =
      match process.commit with
      | [] -> ""
      | l -> "commit\n    " ^ String.concat ",\n    " l ^ ";\n"
    in
    let init = "init\n    " ^ process.init ^ ";\n" in
    let transition =
      match process.trans with
      | [] -> ""
      | l ->
          "trans\n    "
          ^ (List.map transition_to_string l |> String.concat ",\n    ")
          ^ ";"
    in
    Format.asprintf "process task_%s() {\n%s\n}\n" process.name
      (clocks ^ states ^ commit ^ init ^ transition)

  type t = { channels : string list; tasks : process list }

  let to_string xta =
    let tnames = List.map (fun process -> process.name) xta.tasks in
    Format.asprintf "broadcast chan %s;\n\n" (String.concat ", " xta.channels)
    ^ (List.map process_to_string xta.tasks |> String.concat "\n")
    ^ "\n\
       process Obs() {\n\
       /* Write your observer here */\n\
       clock x;\n\
       state\n\
      \    start;\n\
       init\n\
      \    start;\n\
       }\n\n"
    ^ (List.map
         (fun tname -> Format.asprintf "%s = task_%s();" tname tname)
         tnames
      |> String.concat "\n")
    ^ "\nobs = Obs();\n\nsystem\n" ^ String.concat ", " tnames ^ ", obs;\n"
end
