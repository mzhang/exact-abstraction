open Ast
open Rt
open Event

let add_clock filename hpc =
  let ref_process =
    Format.asprintf
      "process Ref() {\\\n\
       clock x;\\\n\
       state\\\n\
      \    hper { x <= %d };\\\n\
       init\\\n\
      \    hper;\\\n\
       }"
      hpc
  in
  let instance = "ref = Ref();" in
  let _ =
    Sys.command
    @@ Format.asprintf
         "sed -i -e '/^\\/\\* process instantiation \\*\\//i %s\\n' -e \
          '/^\\/\\* composition \\*\\//i %s\\n' -e '/^system/a ref,' %s"
         ref_process instance filename
  in
  ()

let write_query filename t jset =
  let query tname sname min max =
    Format.asprintf
      "bounds {%s.%s and %d <= %s.y and %s.y <= %d and ref.hper} : ref.x" tname
      sname min tname tname max
  in
  let queries =
    List.map
      (fun (s, se) ->
        let e1 = List.hd se in
        query t s e1.min e1.max)
      jset
    |> String.concat "\n"
  in
  let oc = open_out filename in
  output_string oc queries;
  close_out oc

let splitByK period hpc l =
  let rec aux l k acc res =
    match l with
    | [] ->
        if k = hpc then List.rev (List.rev acc :: res)
        else failwith @@ "intervals generation fail"
    | [ { Iv.min; max } ] when min = max && min = hpc ->
        List.rev (List.rev acc :: res)
    | x :: l ->
        if x.max <= k then aux l k (x :: acc) res
        else aux l (k + period) [ x ] (List.rev acc :: res)
  in
  aux l period [] []

let compute_task_intervals filename core hpc period t jset =
  let filename_format suffixe extension =
    Format.asprintf "%s%s_%d.%s" filename suffixe core extension
  in
  let xta_filename = filename_format "" "xta" in
  let xta_clock_filename = filename_format "_clock" "xta" in
  let cp =
    Sys.command @@ Format.asprintf "cp %s %s" xta_filename xta_clock_filename
  in
  if cp <> 0 then failwith @@ Format.sprintf "%s file not found" xta_filename;
  add_clock xta_clock_filename hpc;
  let query_filename = filename_format "_intervals" "q" in
  let result_filename = filename_format "_intervals" "res" in
  write_query query_filename t jset;
  let _ =
    Sys.command
    @@ Format.asprintf "verifyta.sh -T -C -s %s %s > %s" xta_clock_filename
         query_filename result_filename
  in
  let intervals_string =
    (* intervals from result_filename are parsed to string *)
    Parse.parse_file Parser.query_results Lexer.query_result result_filename
  in
  (* parses interval string to Iv list *)
  let iv_list =
    List.map
      (Parse.parse_string Parser.intervals Lexer.intervals)
      intervals_string
  in
  (* splits intervals on task period *)
  List.map (splitByK period hpc) iv_list

let compute_interval filename affinity rt event =
  let hpc = cores_hp affinity rt in
  List.fold_left
    (fun acc (t, jset) ->
      let core = List.assoc t affinity in
      let task = List.assoc t rt in
      let period = task.period in
      let hpc = IntMap.find core hpc in
      (* intervals[s][k] *)
      let intervals = compute_task_intervals filename core hpc period t jset in
      (* first event of each segment *)
      let event_index =
        List.map
          (fun (s, se) ->
            let e1 = List.hd se in
            (s, e1.name))
          jset
      in
      (* converts list to map with additionnal key event *)
      let iv =
        List.fold_left2
          (fun acc (s, e1) iv ->
            StringMap.add s (StringMap.singleton e1 iv) acc)
          StringMap.empty event_index intervals
      in
      StringMap.add t iv acc)
    StringMap.empty event
