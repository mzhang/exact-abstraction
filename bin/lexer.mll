{
  open Lexing
  open Parser
}

let newline = '\n' | '\r' | "\r\n"
let blank   = [' ' '\t' '\r']

let digit    = ['0'-'9']
let alpha    = ['a'-'z' 'A'-'Z']
let alphanum = alpha | digit | '_'

let identifier = alpha alphanum*

rule rt = parse
  | newline { new_line lexbuf; rt lexbuf }
  | blank+  { rt lexbuf                  }

  | (identifier as tname) "_per" blank+ (digit+ as period)            { PERIOD (tname, int_of_string period) }
  | (identifier as sname) "_succ_" digit+ blank+ (identifier as succ) { SUCC (sname, succ)                   }
  | "CS_" (digit+) "_1_1" blank+ (identifier as sname)                { SUCC ("act", sname)                  }

  | identifier as id { ID id                 }
  | digit+ as d      { NUM (int_of_string d) }

  | eof            { EOF       }
  | [^ '\n' '\r']* { rt lexbuf }

and affinity = parse
  | newline            { new_line lexbuf; affinity lexbuf }
  | blank+             { affinity lexbuf                  }
  | "#" [^ '\n' '\r']* { affinity lexbuf                  }

  | "affinity_" (identifier as tname) { AFFINITY tname        }
  | digit+ as d                       { NUM (int_of_string d) }

  | eof    { EOF }
  | _ as c { failwith (Printf.sprintf "unexpected character: %c" c) }

and event = parse
  | newline { new_line lexbuf; event lexbuf }
  | blank+  { event lexbuf                  }

  | "task" { TASK     }
  | "("    { LPAREN   }
  | ")"    { RPAREN   }
  | "["    { LBRACKET }
  | "]"    { RBRACKET }
  | ","    { COMMA    }

  | identifier as id { ID id                 }
  | digit+ as d      { NUM (int_of_string d) }

  | eof    { EOF }
  | _ as c { failwith (Printf.sprintf "unexpected character: %c" c) }

and query_result = parse
  | newline { new_line lexbuf; query_result lexbuf }
  | blank+  { query_result lexbuf                  }

  | "ref.x:" blank+ ([^ '\n' '\r']* as intervals) { INTERVALS intervals }

  | eof { EOF                 }
  | _   { query_result lexbuf }

and intervals = parse
  | "[" { LBRACKET }
  | "]" { RBRACKET }
  | "," { COMMA    }

  | digit+ as d { NUM (int_of_string d) }

  | eof    { EOF }
  | _ as c { failwith (Printf.sprintf "unexpected character: %c" c) }
