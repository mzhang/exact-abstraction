open Lexing
open Parser

let parse parser lexer lex =
  try parser lexer lex with
  | Error ->
      let p = lex.lex_curr_p in
      let col = p.pos_cnum - p.pos_bol in
      let msg =
        Format.asprintf "Parser error in file %s, line %d, column %d"
          p.pos_fname p.pos_lnum col
      in
      failwith msg
  | Failure err ->
      let p = lex.lex_curr_p in
      let col = p.pos_cnum - p.pos_bol in
      let msg =
        Format.asprintf "Lexer error in file %s, line %d, column %d: %s"
          p.pos_fname p.pos_lnum col err
      in
      failwith msg

let parse_file parser lexer filename =
  let lex = from_channel (open_in filename) in
  lex.lex_curr_p <- { lex.lex_curr_p with pos_fname = filename };
  parse parser lexer lex

let parse_string parser lexer str =
  let lex = from_string str in
  parse parser lexer lex
