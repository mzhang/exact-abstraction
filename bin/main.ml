let verbose = ref false

let parse_args () =
  let usage_msg =
    "usage: abstraction_gen.exe [-verbose] [-xta] [-force] <project> <event>"
  in
  let input = ref [] in
  let xta = ref false in
  let force = ref false in
  let anon_fun arg = input := arg :: !input in
  let speclist =
    [
      ("-verbose", Arg.Set verbose, "output debug information");
      ("-xta", Arg.Set xta, "uses already generated xta files");
      ("-force", Arg.Set force, "ignore job without production error");
    ]
  in
  Arg.parse speclist anon_fun usage_msg;
  match !input with
  | [ event; project ] ->
      let event = "examples/" ^ event in
      let rt = "examples/" ^ project ^ ".rt" in
      let affinity = "examples/" ^ project ^ ".sol" in
      if not (Sys.file_exists event) then
        invalid_arg (Format.sprintf "%s file not found" event);
      if not (Sys.file_exists rt) then
        invalid_arg (Format.sprintf "%s file not found" rt);
      if not (Sys.file_exists affinity) then
        invalid_arg (Format.sprintf "%s file not found" affinity)
      else (project, event, !xta, !force)
  | _ -> invalid_arg usage_msg

let print str = if !verbose then print_endline str

let _ =
  try
    let project, event, xta, force = parse_args () in
    if not xta then (
      print "\nxta generation";
      let xta_generation = Sys.command ("./xta_gen.sh " ^ project) in
      if xta_generation <> 0 then failwith "xta generation fail");
    let project = "examples/" ^ project in
    print "\nparsing rt";
    let rt = Parse.parse_file Parser.rt Lexer.rt (project ^ ".rt") in
    print (Ast.Rt.show rt);
    print "\nparsing affinity";
    let affinity =
      Parse.parse_file Parser.affinity Lexer.affinity (project ^ ".sol")
    in
    print (Ast.Affinity.show affinity);
    print "\nparsing event";
    let event = Parse.parse_file Parser.events Lexer.event event in
    print (Ast.Event.show event);
    let _ =
      Checker.check_event event rt force;
      Checker.check_distinct_affinity event affinity
    in
    print "\nintervals computation";
    let iv = Interval.compute_interval project affinity rt event in
    print (Ast.Iv.show iv);
    print "\nabstraction generation";
    let xta = Abstraction.abstraction affinity rt event iv in
    let output_file = Format.asprintf "%s_abstraction.xta" project in
    let oc = open_out output_file in
    Printf.fprintf oc "%s" (Ast.Xta.to_string xta);
    flush oc;
    close_out oc
  with
  | Invalid_argument err -> Printf.eprintf "Invalid argument.\n%s\n" err
  | Failure err -> Printf.eprintf "Error: %s\n" err
