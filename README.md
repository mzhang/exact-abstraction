# Exact abstraction

This artifact was produced for the paper "Scalable Computation of Inter-Core Bounds Through Exact Abstractions", to appear in the proceedings of COMPSAC 2024.

## What the artefact provides w.r.t the paper

The artifact implements the paper's algorithms. It also provides ready-to-use abstractions from the WATERS 2017 case study.

##  Contents

The repository includes:

- `bin/`: source code to generate the exact abstraction from an ERTS description (.rt), an affinity (.sol), a description of events, and optionally ERTS's translation in Uppaal input format (.xta)

- `Makefile`: build the exact abstraction generator

- `dune-project`: to build using dune

- `examples/`: `event_waters.txt` is an example of events' description for the WATERS 2017 industrial challenge used in the paper. If you want to skip the generation step for the WATERS example in the paper, `waters_ff.xta` and `waters_lf.xta` are provided with an observer for FF and LF bounds respectively, and `waters.q` is the query for both generated examples


- `README.md`: this file

## Installation and usage

First, to compute bounds you need to download UPPAAL 5.1.0 or a later version under free academic licence here <https://uppaal.org/downloads/>.

### Using the full toolchain

#### Installation

- Add the path to `uppaal-version/bin` to your `PATH`
- You need **OCaml** version 4.13 or later installed with **opam**, OCaml's package manager. You can download it here <https://opam.ocaml.org/doc/Install.html>. After installing **opam**, make sure to run `opam init` to install **OCaml**.
- You need **dune** version 3.0 or later, **menhir** and **ppx_deriving** installed with **opam**: run `opam install dune menhir ppx_deriving`
- Download the FHZ's xta generator from <https://gitlab.math.univ-paris-diderot.fr/foughali/fhz_aeic-jsa>
- Download this repository in `fhz_aeic-jsa/Artefact/`
- Go to `fhz_aeic-jsa/Artefact/exact-abstraction/` and run `make`, this creates `abstraction_gen.exe` in `fhz_aeic-jsa/Artefact/`

#### Abstraction generation

Go to `fhz_aeic-jsa/Artefact/` and run

```
./abstraction_gen.exe <filename1> <filename2>
```

where `<filename1>.rt` is an ERTS description, `<filename1>.sol` is an affinity, `<filename2>` describes events, and the files are in `fhz_aeic-jsa/Artefact/examples/`.

The exact abstraction `<filename1>_abstraction.xta` is generated in `fhz_aeic-jsa/Artefact/examples/`


The available options are :

- `-verbose`: prints parsed data from input files and the computed intervals
- `-xta`: uses existing generated `xta` files in `fhz_aeic-jsa/Artefact/examples/`
- `-force`: allows to generate the abstraction if there is a job that contains no event-producing segment in multiple-job task that produces an event

The process is fully automated, but you need to manually adapt the observer TA based on your needs (within the .xta file or using UPPAAL's GUI). 

#### Computing bounds

- Open the generated abstraction in UPPAAL
- Select **Obs** in the Editor tab to write your observer
- Write your query in the Verifier tab or import a query file
- In the Verifier tab, select the query and click on **Check**

### Skipping the generation for the paper's case study

If you only want to re-compute the bounds for the WATERS case study as in the paper, without generating the abstractions: 

- Open the ready-to-use abstractions `waters_ff.xta` or `waters_lf.xta` in which the observers are already adapted for FF or LF bounds
- Load `waters.q`
- In the Verifier tab, select a query and click on **Check**

## License

The code was produced by Mohammed Foughali and Maryline Zhang and is licensed under CeCILL-B.
