.PHONY: build

build:
	dune build
	ln -f ./_build/default/bin/main.exe ../abstraction_gen.exe

clean:
	rm -f ../abstraction_gen.exe
	dune clean
